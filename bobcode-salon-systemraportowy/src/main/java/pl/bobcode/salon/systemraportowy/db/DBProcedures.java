package pl.bobcode.salon.systemraportowy.db;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public enum DBProcedures {

	SQL_BEST_CARS,
	SQL_BEST_SALONS,
	SQL_CURRENT_ORDERS;

	private static String PREFIX = "USE [SALON] EXEC ";
	
	private static String CARS = PREFIX + "[dbo].[usp_GetCarSales]";
	private static String ORDERS = PREFIX + "[dbo].[usp_GetAllOrdersWithAllParameters]";
	private static String SALONS = PREFIX + "[dbo].[usp_GetSalonSales]";

	public static String getQuerry(DBProcedures q) {
		switch (q) {
		case SQL_BEST_SALONS:
			return SALONS;
		case SQL_BEST_CARS:
			return CARS;
		case SQL_CURRENT_ORDERS:
			return ORDERS;
		default:
			return null;
		}
	}

}
