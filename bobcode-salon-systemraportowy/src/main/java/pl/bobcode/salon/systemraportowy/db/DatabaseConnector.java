package pl.bobcode.salon.systemraportowy.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public class DatabaseConnector {

	private final String URL = "jdbc:sqlserver://localhost\\EVE-PC:1433;databaseName=Salon;integratedSecurity=true";

	private Connection con = null;;
	private Statement sta = null;;
	private ResultSet rs = null;;

	public DatabaseConnector() throws SQLException {
		System.setProperty("java.net.preferIPv6Addresses", "true");
		con = DriverManager.getConnection(URL);
	}

	public ResultSet getResultSet(DBProcedures q) throws SQLException {
		String SPsql = DBProcedures.getQuerry(q);
		PreparedStatement ps = con.prepareStatement(SPsql);
		ps.setEscapeProcessing(true);
		ps.setQueryTimeout(10);
		//ps.setString(1, <param1>);
		//ps.setString(2, <param2>);
		rs = ps.executeQuery();
		return rs;
	}

	public void closeDBConnection() {
		try {
			if (rs != null)
				rs.close();
			if (sta != null)
				sta.close();
			if (con != null)
				con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
