package pl.bobcode.salon.systemraportowy.controller;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import pl.bobcode.salon.systemraportowy.db.DBProcedures;
import pl.bobcode.salon.systemraportowy.db.DatabaseConnector;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public class OrderReportGenerator implements ReportGenerable {

	public void generate() {

		System.out.println("Generowanie raportu aktualnych zam�wie�");

		PDFConfig pdf = null;
		try {
			pdf = new PDFConfig();
			PDPageContentStream stream = pdf.getStream();

			// Nag��wek
			stream.setFont(PDType1Font.TIMES_BOLD, 20);
			stream.beginText();
			stream.moveTextPositionByAmount(100, 700);

			stream.drawString("Aktualne zam�wienia:");

			// Reszta raportu
			stream.setFont(PDType1Font.TIMES_ROMAN, 10);
			stream.moveTextPositionByAmount(0, -30);

			// Dane z bazy
			DatabaseConnector connector = null;
			try {
				connector = new DatabaseConnector();
				int i = 1;
				ResultSet rs = connector.getResultSet(DBProcedures.SQL_CURRENT_ORDERS);
				while (rs.next()) {
					stream.drawString(i + ". ID=" + rs.getInt("idZam�wienie")
							+ " || data utworzenia="
							+ rs.getDate("dataUtworzenia")
							+ ", typ zam�wienia=" + rs.getString("typZam�wienia"));
					
					stream.moveTextPositionByAmount(0, -20);
					
					String firma = rs.getString("idFirma");
					if (firma == null) {
						firma = "brak firmy";
					}
					stream.drawString("    KLIENT: id=" + rs.getInt("idKlient")
							+ ", nazwa=" + rs.getString("imieKlienta") + " "
							+ rs.getString("nazwiskoKlienta") + ", id(Firma)="
							+ firma);
					
					stream.moveTextPositionByAmount(0, -20);
					
					stream.drawString("    PRACOWNIK: id="
							+ rs.getString("idPracownik") + ", nazwisko="
							+ rs.getString("nazwiskoPracownika"));
					
					stream.moveTextPositionByAmount(0, -20);
					
					stream.drawString("    SAMOCH�D: id="
							+ rs.getInt("idPojazd") + ", cena="
							+ rs.getString("cena") + ", model="
							+ rs.getString("nazwaMarki")
							+ rs.getString("nazwaModelu") + ", lakier="
							+ rs.getString("typLakieru"));
					
					
					stream.moveTextPositionByAmount(0, -40);
					i++;
					
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (connector != null) {
					connector.closeDBConnection();
				}
			}

			// Zapisywanie dokumentu
			stream.endText();
			pdf.closeStream();
			pdf.savePDF("Raport zam�wie�");

			// Dialog box
			System.out.println("Zako�czono generowanie raportu");
			JOptionPane.showMessageDialog(null,
					"Wygenerowano raport aktualnych zam�wie�",
					"Wynik generacji raportu", JOptionPane.PLAIN_MESSAGE);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pdf != null)
				pdf.closePDF();

		}
	}
}
