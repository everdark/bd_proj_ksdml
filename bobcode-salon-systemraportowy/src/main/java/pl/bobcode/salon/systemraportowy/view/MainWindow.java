package pl.bobcode.salon.systemraportowy.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import pl.bobcode.salon.systemraportowy.controller.CarSalesReportGenerator;
import pl.bobcode.salon.systemraportowy.controller.OrderReportGenerator;
import pl.bobcode.salon.systemraportowy.controller.ReportGenerable;
import pl.bobcode.salon.systemraportowy.controller.SalonSalesReportGenerator;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public class MainWindow extends JFrame {

	/**
	 * serial id
	 */
	private static final long serialVersionUID = -8038052398683247082L;

	private static final int DEFAULT_WIDTH = 350;
	private static final int DEFAULT_HEIGHT = 125;
	private static final String TITLE = "System raportowy";

	private JPanel buttonPanel;

	public MainWindow() {
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		setLocationByPlatform(true);
		setTitle(TITLE);
		addButtons();
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Adding functional buttons
	 */
	private void addButtons() {
		JButton orderReportButton = new JButton("Generuj wykaz realizowanych zam�wie�");
		JButton salonReportButton = new JButton("Generuj wykaz sprzedarzy salon�w");
		JButton carReportButton = new JButton("Generuj list� najlepiej sprzedawanych samochod�w");

		buttonPanel = new JPanel();
		buttonPanel.add(orderReportButton);
		buttonPanel.add(salonReportButton);
		buttonPanel.add(carReportButton);

		this.add(buttonPanel);

		ReportAction orderAction = new ReportAction("order");
		ReportAction carAction = new ReportAction("car");
		ReportAction salonAction = new ReportAction("salon");

		orderReportButton.addActionListener(orderAction);
		carReportButton.addActionListener(carAction);
		salonReportButton.addActionListener(salonAction);
	}

	/**
	 * Action Listener class
	 * 
	 * @author Szymon Ligocki - s.ligocki@outlook.com
	 */
	private class ReportAction implements ActionListener {

		private String option;

		public ReportAction(String option) {
			this.option = option;
		}

		public void actionPerformed(ActionEvent e) {

			ReportGenerable generator = null;

			if (option.equals("order")) {
				generator = new OrderReportGenerator();
			} else if (option.equals("car")) {
				generator = new CarSalesReportGenerator();
			} else if (option.equals("salon")) {
				generator = new SalonSalesReportGenerator();
			} else {
				throw new IllegalArgumentException();
			}

			if (generator != null) {
				generator.generate();
			}
		}
	}

	/**
	 * Main window invoker
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MainWindow window = new MainWindow();
				window.setVisible(true);
			}
		});
	}

}
