package pl.bobcode.salon.systemraportowy.controller;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import pl.bobcode.salon.systemraportowy.db.DBProcedures;
import pl.bobcode.salon.systemraportowy.db.DatabaseConnector;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public class CarSalesReportGenerator implements ReportGenerable {

	public void generate() {

		System.out.println("Generowanie raportu sprzedarzy dla samochodów");

		PDFConfig pdf = null;
		try {
			pdf = new PDFConfig();
			PDPageContentStream stream = pdf.getStream();

			// Nagłówek
			stream.setFont(PDType1Font.TIMES_BOLD, 20);
			stream.beginText();
			stream.moveTextPositionByAmount(100, 700);

			stream.drawString("Samochody:");

			// Reszta raportu
			stream.setFont(PDType1Font.TIMES_ROMAN, 10);
			stream.moveTextPositionByAmount(0, -30);

			// Dane z bazy
			DatabaseConnector connector = null;
			try {
				connector = new DatabaseConnector();
				int i = 1;
				ResultSet rs = connector
						.getResultSet(DBProcedures.SQL_BEST_CARS);
				while (rs.next()) {
					stream.drawString(i + ". marka="
							+ rs.getString("nazwaMarki") + ", model="
							+ rs.getString("nazwaModelu") + ", ilosc zamowien="
							+ rs.getInt("iloscZamowien"));

					stream.moveTextPositionByAmount(0, -10);
					i++;

				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (connector != null) {
					connector.closeDBConnection();
				}
			}

			// Zapisywanie dokumentu
			stream.endText();
			pdf.closeStream();
			pdf.savePDF("Raport samochodów");

			// Dialog box
			System.out.println("Zakończono generowanie raportu");
			JOptionPane.showMessageDialog(null,
					"Wygenerowano raport sprzedarzy samochodów",
					"Wynik generacji raportu", JOptionPane.PLAIN_MESSAGE);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pdf != null)
				pdf.closePDF();

		}
	}
}
