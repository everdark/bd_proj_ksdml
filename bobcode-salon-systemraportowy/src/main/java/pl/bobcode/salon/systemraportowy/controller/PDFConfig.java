package pl.bobcode.salon.systemraportowy.controller;

import java.io.IOException;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public class PDFConfig {

	private PDDocument doc;
	private PDPage page;
	private PDPageContentStream stream = null;

	public PDPageContentStream getStream() throws IOException {
		doc = new PDDocument();
		page = new PDPage();
		doc.addPage(page);
		stream = new PDPageContentStream(doc, page);
		return stream;
	}

	public void closeStream() throws IOException {
		if (stream == null) {
			return;
		}
		stream.close();
	}

	public void savePDF(String filename) throws COSVisitorException,
			IOException {
		doc.save(filename + ".pdf");
	}

	public void closePDF() {
		try {
			if (stream != null)
				stream.close();
			if (doc != null)
				doc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
