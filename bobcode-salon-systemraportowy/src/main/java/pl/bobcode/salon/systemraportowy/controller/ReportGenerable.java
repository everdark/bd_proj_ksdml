package pl.bobcode.salon.systemraportowy.controller;

/**
 * @author Szymon Ligocki - s.ligocki@outlook.com
 *
 */
public interface ReportGenerable {

	void generate();
}
